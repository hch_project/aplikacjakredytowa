package TCP;

/**
 * Created by Michal
 */
import java.io.*;
import java.net.*;
import java.util.concurrent.Callable;

import BANK.KalkulatorObliczen;

public class ServerThread implements Callable<String> {
	Socket mySocket;
	String[][] lista;
	String NIU = null;
	static int numQ;

	public ServerThread(Socket socket) {
		super(); // konstruktor klasy Thread
		mySocket = socket;
	}

	public String call() // program w�tku
	{
		lista = new String[BazaDanych.numOfRows][7];
		String in = null;
		System.out.println("Otwarto po��czenie");
		try {
			/* odbieramy i drukujemy ... */
			BufferedReader inFromClient = new BufferedReader(new InputStreamReader(mySocket.getInputStream()));
			PrintWriter outToClient = new PrintWriter(new OutputStreamWriter(mySocket.getOutputStream()));

			while (true) {
				while ((in = inFromClient.readLine()) == null) {
				}
				if (in.substring(0, 3).toLowerCase().equals("niu")) {
					NIU = in.substring(3, in.length());
					System.out.println(NIU);
				}
				//
				if (in.equals("lista")) {
					BazaDanych.saveQueryToTable();
					for (int i = 0; i < BazaDanych.numOfRows; i++) {
						for (int j = 0; j < 7; j++) {
							outToClient.println(BazaDanych.list[i][j]);
						}
					}
					outToClient.flush();
					System.err.println("Wyslano liste");
				}
				if (in.equals("iloscwierszy")) {
					numQ = BazaDanych.numOfRows();
					outToClient.write(numQ);
					outToClient.flush();
					System.err.println("Wyslano " + numQ);
				}
				if (in.equals("ObliczKwoteRat")) {
					String kwota=inFromClient.readLine();
					String wplata=inFromClient.readLine();
					String lR=inFromClient.readLine();
					String promocja=inFromClient.readLine();
					String ubezpieczenie=inFromClient.readLine();
					double kwotaRat = KalkulatorObliczen.obliczKwoteRat(Double.parseDouble(kwota), Integer.parseInt(lR), Double.parseDouble(wplata), Double.parseDouble(promocja), Integer.parseInt(ubezpieczenie));
					kwotaRat *= 100;
					kwotaRat = (double) Math.round(kwotaRat);
					kwotaRat /= 100;

					System.out.println(kwotaRat+" "+NIU);
					outToClient.println(kwotaRat);
					outToClient.flush();
				}
				if (in.equals("ObliczLiczbeRat")) {
					String kwota=inFromClient.readLine();
					String wplata=inFromClient.readLine();
					String kR=inFromClient.readLine();
					String promocja=inFromClient.readLine();
					String ubezpieczenie=inFromClient.readLine();
					int liczbaRat = KalkulatorObliczen.obliczIloscRat(Double.parseDouble(kwota), Double.parseDouble(kR), Double.parseDouble(wplata), Double.parseDouble(promocja), Integer.parseInt(ubezpieczenie));
					outToClient.println(liczbaRat);
					outToClient.flush();
				}
				if (in.equals("ObliczKwoteRatWniosek")) {
					String kwota=inFromClient.readLine();
					String wplata=inFromClient.readLine();
					String lR=inFromClient.readLine();
					String promocja=inFromClient.readLine();
					String ubezpieczenie=inFromClient.readLine();
					double kwotaRat = KalkulatorObliczen.obliczKwoteRatWniosek(Double.parseDouble(kwota), Integer.parseInt(lR), Double.parseDouble(wplata), Double.parseDouble(promocja), Integer.parseInt(ubezpieczenie));
					kwotaRat *= 100;
					kwotaRat = (double) Math.round(kwotaRat);
					kwotaRat /= 100;
					outToClient.println(kwotaRat);
					outToClient.flush();
				}
				if (in.equals("SprawdzStatus")) {
					String id=inFromClient.readLine();
					String dochod=inFromClient.readLine();
					String kwotaRaty=inFromClient.readLine();
					String dochodDodatkwoy=inFromClient.readLine();
					String koszta=inFromClient.readLine();
					String iloscOsob=inFromClient.readLine();
					String status = KalkulatorObliczen.wydanaZgoda(Integer.parseInt(id),Double.parseDouble(dochod), Double.parseDouble(kwotaRaty), Double.parseDouble(dochodDodatkwoy), Double.parseDouble(koszta), Integer.parseInt(iloscOsob));
					outToClient.println(status);
					outToClient.flush();
				}
				if (in.equals("exit")) {
					mySocket.close();
					inFromClient.close();
					outToClient.close();
				}
			}

		} catch (Exception e) {
			System.err.println(e);
		}
		return NIU;
	}
}
