package TCP;
/**
 * Created by Michał
 */

import java.net.ServerSocket;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class BazaDanych extends Thread {

	public static Statement s;
	public static Connection connection;
	public static int numOfRows;
	public static String list[][];
	public static ServerSocket serverSocket;

	static int log=0; 

	/**
	 * Metoda ładuje sterownik jdbc
	 *
	 * @return true/false
	 */
	static boolean ladujSterownik() {
		// LADOWANIE STEROWNIKA
		System.out.print("Sprawdzanie sterownika:");
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			return true;
		} catch (Exception e) {
			System.out.println("Blad przy ladowaniu sterownika bazy!");
			return false;
		}
	}

	/**
	 * Metoda służy do nawiązania połączenia z bazą danych
	 *
	 * @param adress
	 *            - adres bazy danych
	 * @param dataBaseName
	 *            - nazwa bazy
	 * @param userName
	 *            - login do bazy
	 * @param password
	 *            - hasło do bazy
	 * @return - połączenie z bazą
	 */
	private static Connection connectToDatabase(String adress, String dataBaseName, String userName, String password) {
		System.out.print("\nLaczenie z baza danych:");
		String baza = "jdbc:mysql://" + adress + "/" + dataBaseName;
		java.sql.Connection connection = null;
		try {
			connection = DriverManager.getConnection(baza, userName, password);
		} catch (SQLException e) {
			System.out.println("Blad przy ladowaniu sterownika bazy!");
			System.exit(1);
		}
		return connection;
	}

	public static int executeUpdate(Statement s, String sql) {
		try {
			return s.executeUpdate(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return -1;
	}

	static void saveQueryToTable() {
		numOfRows = numOfRows();
		list = new String[numOfRows][7];
		ResultSet r = executeQuery(s, "Select * from lista");
		try {
			int num = 0;
			while (r.next()) {
				for (int i = 1; i <= 7; i++) {
					Object obj = r.getObject(i);
					if (obj != null) {
						if (i == 1) {
							String temp = obj.toString();
							list[num][0] = temp;
							num++;
						} else if (i != 1) {
							String temp = obj.toString();
							list[num - 1][i - 1] = temp;
						}
					} else
						System.out.print("\t");
				}
				// System.out.println();
			}
		} catch (SQLException e) {
			System.out.println("Bl�d odczytu z bazy! " + e.toString());
			System.exit(3);
		}
	}

	public static int numOfRows() {
		ResultSet result = executeQuery(s, "SELECT count(id_wniosku) FROM lista;");
		try {
			result.next();
			numOfRows = Integer.parseInt(result.getObject(1).toString());
		} catch (SQLException e) {
			System.out.println("B��d przy pobieraniu ilo�ci wierszy z bazy " + e.toString());
			System.exit(3);
		}
		return numOfRows;
	}
	
	public static void changeStatus(String id_wniosku) {
		executeUpdate(s, "update lista set status='Zatwierdzono' where id_wniosku='" + id_wniosku + "'");
	}
	
	public static void changeButton(String id_wniosku) {
		executeUpdate(s, "update lista set zatwierdz='Zatwierdz' where id_wniosku='" + id_wniosku + "'");
	}
	
	public static void delete(String id_wniosku) {
		executeUpdate(s, "delete from lista where id_wniosku='" + id_wniosku + "'");
	}

	/**
	 * Wykonanie kwerendy i przesłanie wyników do obiektu ResultSet
	 *
	 * @param s
	 *            - Statement
	 * @param sql
	 *            - zapytanie
	 * @return wynik
	 */
	private static ResultSet executeQuery(Statement s, String sql) {
		try {
			return s.executeQuery(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * tworzenie obiektu Statement przesyłającego zapytania do bazy connection
	 *
	 * @param connection
	 *            - połączenie z bazą
	 * @return obiekt Statement przesyłający zapytania do bazy
	 */
	private static Statement createStatement(Connection connection) {
		try {
			return connection.createStatement();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * Zamykanie połączenia z bazą danych
	 *
	 * @param connection
	 *            - połączenie z bazą
	 * @param s
	 *            - obiekt przesyłający zapytanie do bazy
	 */
	static void closeConnection(Connection connection, Statement s) {
		System.out.print("\nZamykanie polaczenia z baz��:");
		try {
			s.close();
			connection.close();
		} catch (SQLException e) {
			System.out.println("B��d przy zamykaniu po��czenia z baz� " + e.toString());
			System.exit(4);
		}
		System.out.print(" Zamkni�cie OK");
	}

	public static boolean dodajWniosek(String wniosek[]) {
		try {
			PreparedStatement statement = connection.prepareStatement(
					"INSERT INTO lista (id_wniosku, nazwisko, imie, pesel, data, status, zatwierdz, adresZamieszkania, numerDowodu, telefon, adresEmail, osobyNaUtrzymaniu, "
							+ "zakladPracy, rodzajZatrudnienia, telefonPraca, stanowisko, zarobkiNetto, okresUmowy, dochodDodatkowy, kosztaDodatkowe, sklep, sprzet, model, cena, "
							+ "wplataWlasna, ubezpieczenie, iloscRat, promocja) "
							+ "values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
			statement.setInt(1, Integer.parseInt(wniosek[0]));
			statement.setString(2, wniosek[1]);
			statement.setString(3, wniosek[2]);
			statement.setString(4, wniosek[3]);
			statement.setString(5, wniosek[4]);
			statement.setString(6, "Oczekiwanie");
			statement.setString(7, "Wyslij");
			statement.setString(8, wniosek[5]);
			statement.setString(9, wniosek[6]);
			statement.setString(10, wniosek[7]);
			statement.setString(11, wniosek[8]);
			statement.setInt(12, Integer.parseInt(wniosek[9]));
			statement.setString(13, wniosek[10]);
			statement.setString(14, wniosek[11]);
			statement.setString(15, wniosek[12]);
			statement.setString(16, wniosek[13]);
			statement.setDouble(17, Double.parseDouble(wniosek[14]));
			statement.setString(18, wniosek[15]);
			statement.setDouble(19, Double.parseDouble(wniosek[16]));
			statement.setDouble(20, Double.parseDouble(wniosek[17]));
			statement.setString(21, wniosek[18]);
			statement.setString(22, wniosek[19]);
			statement.setString(23, wniosek[20]);
			statement.setDouble(24, Double.parseDouble(wniosek[21]));
			statement.setDouble(25, Double.parseDouble(wniosek[22]));
			statement.setInt(26, Integer.parseInt(wniosek[23]));
			statement.setInt(27, Integer.parseInt(wniosek[24]));
			statement.setDouble(28, Double.parseDouble(wniosek[25]));
			statement.execute();
		} catch (SQLException e) {
			System.err.println("Blad przy wstawianiu sprzetu");
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public static String[] pobierzDane(int id_wniosku, String[] Q){
		ResultSet result = executeQuery(s, "SELECT zarobkiNetto,dochodDodatkowy,kosztaDodatkowe,osobyNaUtrzymaniu,cena,iloscRat,wplataWlasna,promocja,ubezpieczenie FROM lista where id_wniosku='"+id_wniosku+"';");
		try {
			while (result.next()) {
				int num=0;
				for (int i = 1; i <= 9; i++) {
					Object obj = result.getObject(i);
					if (obj != null) {
							String temp = obj.toString();
							Q[num] = temp;
							num++;
					} else
						System.out.print("\t");
				}
			}
		} catch (SQLException e) {
			System.out.println("B��d przy pobieraniu ilo�ci wierszy z bazy " + e.toString());
			System.exit(3);
		}
		return Q;
	}
	
	

	// public static void main(String args[]) {
	public void run() {

		if (ladujSterownik())
			System.out.print(" sterownik OK");
		else
			System.exit(1);

		connection = connectToDatabase("127.0.0.1", "", "root", "");
		s = createStatement(connection);
		if (connection != null)
			System.out.print(" polaczenie OK\n");

		// WYKONYWANIE OPERACJI NA BAZIE DANYCH
		if (executeUpdate(s, "USE AplikacjaKredytowa;") != -1)
			System.out.println("Baza wybrana");
		else
			System.out.println("Baza niewybrana!");
		System.out.println("Pobieranie danych z bazy:");

		saveQueryToTable();

		for (int i = 0; i < numOfRows; i++) {
			System.out.println("");
			for (int j = 0; j < 7; j++) {
				System.out.print(list[i][j] + " ");
			}
		}
		System.out.println();
	}
}
