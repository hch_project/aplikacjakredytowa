package BANK;

import java.util.concurrent.TimeUnit;

import javax.swing.UnsupportedLookAndFeelException;

import TCP.BazaDanych;

public class KalkulatorObliczen {

	public static int obliczIloscRat(double kwota, double kwotaRaty, double wplata, double promocja,
			int ubezpieczenie) throws ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException {
		double ir = (((kwota - wplata) + (ubezpieczenie * (kwota - wplata))/100) / (kwotaRaty - promocja * kwota));
		int iloscRat = (int) Math.ceil(ir);
		return iloscRat;
	}

	public static double obliczKwoteRat(double kwota, int liczbaRat, double wplata, double promocja,
			int ubezpieczenie) throws ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException {
		double kwotaRaty = ((kwota - wplata) * (promocja * liczbaRat + 1) + ((kwota - wplata) * ubezpieczenie / 100))
				/ liczbaRat;
		return kwotaRaty;
	}
	
	public static double obliczKwoteRatWniosek(double kwota, int liczbaRat, double wplata, double promocja,
			int ubezpieczenie) {
		try {
			TimeUnit.SECONDS.sleep(10);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		double kwotaRaty = ((kwota - wplata) * (promocja * liczbaRat + 1) + ((kwota - wplata) * ubezpieczenie / 100))
				/ liczbaRat;
		return kwotaRaty;
	}
	
	public static String wydanaZgoda(int id, double dochod, double kwotaRaty, double dochodDodatkowy, double koszta, int iloscOsob){
		String status="";
		double wolneKoszty=dochod+dochodDodatkowy-koszta-iloscOsob*250;
		if((wolneKoszty*0.6)>kwotaRaty)
			status="Zaakceptowano";
		else
			status="Odrzucono";		
		BazaDanych.executeUpdate(BazaDanych.s, "update lista set status='"+status+"' where id_wniosku='" + id + "'");
		return status;
	}
}
