package BAZA;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DBMySQL {

	static int log=0; 
	static {
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException ex) {

		}
	}

	public static Connection getConnection() throws SQLException {
		return DriverManager.getConnection("jdbc:mysql://localhost:3306/logowanie", "root", "");
	}

	public static void closeConnection(Connection connection) {
		if (connection == null)
			return;
		try {
			connection.close();
		} catch (SQLException ex) {

		}
	}
	
	public static void connect(String sql) {
		Connection connection = null;
		try {
			connection = getConnection();
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.executeUpdate();
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			closeConnection(connection);
		}
	}
	
	public static boolean checkUser(String login, String haslo) {
		String sql = "select count(*) from logowanie where login='" + login + "' and haslo = '" + haslo + "'";

		Connection connection = null;
		try {
			connection = getConnection();
			PreparedStatement statement = connection.prepareStatement(sql);
			ResultSet result = statement.executeQuery();
			result.next();
			log = Integer.parseInt(result.getObject(1).toString());
		} catch (Exception e) {
			System.out.println("B��d przy pobieraniu ilo�ci uzytkownikow z bazy " + e.toString());
			System.exit(3);
		} finally {
			closeConnection(connection);
		}
		if (log == 1) {
			return true;
		} else {
			return false;
		}
	}
}