package GUI;

import java.awt.*;
import javax.swing.*;
import javax.swing.table.TableColumn;
import TCP.BazaDanych;
import TCP.Server;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.PrintWriter;
import java.net.Socket;
import net.miginfocom.swing.MigLayout;

public class Client extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	static JFrame frame;
	static JTextField textField;
	static String login;
	static String haslo;
	public static JLabel lblNewLabel;
	static JButton btnZaloguj;
	static JPasswordField passwordField;
	static String[][] lista;
	static int numOfRows;
	static TableColumn tc;
	static String[] Q;

	static Thread serwer, client;
	static Socket socket;
	static BufferedReader inFromServer;
	static PrintWriter outToServer;

	/**
	 * Launch the application.
	 */
	public static void main(String args[]) {

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					new Client();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

	}

	/**
	 * Create the frame.
	 */
	public Client() throws ClassNotFoundException, InstantiationException, IllegalAccessException,
			UnsupportedLookAndFeelException {
		frame = initialize();
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setVisible(true);
	}

	public static void close() {
		frame.setVisible(false);
	}

	private JFrame initialize() throws ClassNotFoundException, InstantiationException, IllegalAccessException,
			UnsupportedLookAndFeelException {
		UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");

		frame = new JFrame("HCHLogIn");
		frame.getContentPane().setBackground(new Color(25, 25, 25));
		frame.setBounds(300, 10, 1000, 800);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(new MigLayout("", "[340.00px,grow][250.00px][230.00][170.00px,grow]", "[56px][21px][48px][21px][30.00px][50.00px][30.00px][30.00][50.00][][][55.00][55.00][][62.00][][][][]"));

		lblNewLabel = new JLabel("");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setFont(new Font("Bookman Old Style", Font.BOLD | Font.ITALIC, 24));
		lblNewLabel.setForeground(new Color(249, 0, 0));
		lblNewLabel.setBackground(new Color(255, 255, 255));
		frame.getContentPane().add(lblNewLabel, "cell 0 0 4 1,grow");

		JLabel lblLogin = new JLabel("LOGIN");
		lblLogin.setForeground(new Color(204, 204, 204));
		lblLogin.setFont(new Font("Bookman Old Style", Font.BOLD | Font.ITALIC, 24));
		frame.getContentPane().add(lblLogin, "cell 1 4,grow");

		textField = new JTextField();
		textField.setHorizontalAlignment(SwingConstants.CENTER);
		textField.setFont(new Font("Bookman Old Style", Font.BOLD | Font.ITALIC, 20));
		textField.setForeground(new Color(0, 0, 0));
		textField.setBackground(new Color(204, 204, 204));
		frame.getContentPane().add(textField, "cell 1 5,grow");
		textField.setColumns(10);

		JLabel lblHaso = new JLabel("HAS�O");
		lblHaso.setForeground(new Color(204, 204, 204));
		lblHaso.setFont(new Font("Bookman Old Style", Font.BOLD | Font.ITALIC, 24));
		frame.getContentPane().add(lblHaso, "cell 1 7,grow");

		passwordField = new JPasswordField();
		passwordField.setHorizontalAlignment(SwingConstants.CENTER);
		passwordField.setForeground(new Color(0, 0, 0));
		passwordField.setFont(new Font("Bookman Old Style", Font.BOLD | Font.ITALIC, 27));
		passwordField.setBackground(new Color(204, 204, 204));
		frame.getContentPane().add(passwordField, "cell 1 8,grow");

		btnZaloguj = new JButton("Zaloguj");
		btnZaloguj.setFont(new Font("Bookman Old Style", Font.BOLD | Font.ITALIC, 24));
		btnZaloguj.setForeground(new Color(0, 0, 0));
		btnZaloguj.setBackground(new Color(102, 102, 102));
		btnZaloguj.setMnemonic(KeyEvent.VK_ENTER);
		btnZaloguj.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent arg0) {
				login = textField.getText();
				haslo = passwordField.getText();
				AplikacjaKredytowa.connect(login, haslo);
			}
		});
		frame.getContentPane().add(btnZaloguj, "cell 2 12,grow");

		JButton btnNewButton = new JButton("Po��cz z Bankiem");
		btnNewButton.setForeground(new Color(0, 0, 0));
		btnNewButton.setFont(new Font("Bookman Old Style", Font.BOLD | Font.ITALIC, 20));
		btnNewButton.setBackground(new Color(102, 102, 102));
		btnNewButton.setMnemonic(KeyEvent.VK_B);
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Client.lblNewLabel.setText("");
				serwer = new Server();
				serwer.start();
			}
		});
		frame.getContentPane().add(btnNewButton, "cell 2 18,grow");
		return frame;
	}
}
