package GUI;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import TCP.BazaDanych;

import net.miginfocom.swing.MigLayout;
import java.awt.Color;
import javax.swing.JComboBox;

public class WniosekPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private JTextField textField_6;
	private JTextField textField_7;
	private JTextField textField_8;
	private JTextField textField_9;
	private JTextField textField_10;
	private JTextField textField_11;
	private JTextField textField_12;
	private JTextField textField_13;
	private JTextField textField_14;
	private JTextField textField_15;
	private JTextField textField_16;
	private JTextField textField_17;
	private JTextField textField_18;
	private JTextField textField_19;
	private JTextField textField_20;
	private JTextField textField_22;
	private JComboBox<String> comboBox, comboBox_1;
	private int ubezpieczenie;
	private double promocja;

	static JLabel lblNewLabel_1 = new JLabel("");
	static JLabel label_15 = new JLabel("");
	static JLabel lblNewLabel = new JLabel("");
	static JLabel lblNewLabel_8 = new JLabel("");
	static JLabel lblNewLabel_2 = new JLabel("");
	static JLabel lblNewLabel_9 = new JLabel("");
	static JLabel lblNewLabel_3 = new JLabel("");
	static JLabel lblNewLabel_10 = new JLabel("");
	static JLabel lblNewLabel_4 = new JLabel("");
	static JLabel lblNewLabel_11 = new JLabel("");
	static JLabel lblNewLabel_5 = new JLabel("");
	static JLabel lblNewLabel_12 = new JLabel("");
	static JLabel lblNewLabel_6 = new JLabel("");
	static JLabel lblNewLabel_13 = new JLabel("");
	static JLabel lblNewLabel_7 = new JLabel("");
	static JLabel lblNewLabel_14 = new JLabel("");
	static JLabel lblNewLabel_15 = new JLabel("");
	static JLabel lblNewLabel_19 = new JLabel("");
	static JLabel lblNewLabel_16 = new JLabel("");
	static JLabel lblNewLabel_17 = new JLabel("");
	static JLabel lblNewLabel_20 = new JLabel("");
	static JLabel lblNewLabel_18 = new JLabel("");

	/**
	 * Create the panel.
	 * 
	 * @throws UnsupportedLookAndFeelException
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 * @throws ClassNotFoundException
	 */
	public WniosekPanel() throws ClassNotFoundException, InstantiationException, IllegalAccessException,
			UnsupportedLookAndFeelException {
		setForeground(new Color(204, 204, 204));
		setBackground(new Color(25, 25, 25));
		UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
		setBounds(0, 0, 960, 870);
		// setPreferredSize(new Dimension(800, 1000));
		setLayout(new MigLayout("", "[100.00,grow][250.00px][100.00][250.00px][100.00px,grow]",
				"[57.00][50px][][][42.00px][-23.00px][][][][24px][20px][24px][20px][24px][20px][24px][20px][24px][20px][24px][20px][24px][20px][24px][20px][24.00][27.00][19.00][32px][][][][24px][20px][24px][20px][24px][20px][24px][20px][][][][][][][][][][39px]"));

		JLabel label_14 = new JLabel("WNIOSEK KREDYTOWY");
		label_14.setForeground(new Color(204, 204, 204));
		label_14.setHorizontalAlignment(SwingConstants.CENTER);
		label_14.setFont(new Font("Bookman Old Style", Font.BOLD | Font.ITALIC, 40));
		add(label_14, "flowx,cell 1 1 3 1,growx,aligny top");

		JLabel label_18 = new JLabel("DANE WNIOSKODAWCY\r\n");
		label_18.setForeground(new Color(204, 204, 204));
		label_18.setHorizontalAlignment(SwingConstants.CENTER);
		label_18.setFont(new Font("Bookman Old Style", Font.BOLD | Font.ITALIC, 25));
		add(label_18, "cell 1 4 3 1,growx");

		// STRONA LEWA
		JLabel lblPesel1 = new JLabel("PESEL");
		lblPesel1.setForeground(new Color(204, 204, 204));
		lblPesel1.setFont(new Font("Bookman Old Style", Font.ITALIC, 18));
		add(lblPesel1, "cell 1 9,growx,aligny top");

		// STRONA PRAWA
		JLabel label_5 = new JLabel("Zak�ad pracy");
		label_5.setForeground(new Color(204, 204, 204));
		label_5.setFont(new Font("Bookman Old Style", Font.ITALIC, 18));
		add(label_5, "cell 3 9,growx,aligny top");

		label_15.setForeground(new Color(204, 204, 204));
		label_15.setFont(new Font("Bookman Old Style", Font.BOLD, 12));
		add(label_15, "cell 0 10,alignx trailing");

		textField = new JTextField();
		textField.setFont(new Font("Bookman Old Style", Font.BOLD, 15));
		textField.setBackground(new Color(204, 204, 204));
		textField.setColumns(10);
		add(textField, "cell 1 10,growx,aligny top");
		textField_8 = new JTextField();
		textField_8.setFont(new Font("Bookman Old Style", Font.BOLD, 15));
		textField_8.setBackground(new Color(204, 204, 204));
		textField_8.setColumns(10);
		add(textField_8, "cell 3 10,growx,aligny top");

		lblNewLabel.setForeground(new Color(204, 204, 204));
		lblNewLabel.setFont(new Font("Bookman Old Style", Font.BOLD, 12));
		add(lblNewLabel, "cell 4 10");

		JLabel lblImie = new JLabel("Imi�");
		lblImie.setForeground(new Color(204, 204, 204));
		lblImie.setFont(new Font("Bookman Old Style", Font.ITALIC, 18));
		add(lblImie, "cell 1 11,growx,aligny top");

		JLabel label_6 = new JLabel("Rodzaj zatrudnienia");
		label_6.setForeground(new Color(204, 204, 204));
		label_6.setFont(new Font("Bookman Old Style", Font.ITALIC, 18));
		add(label_6, "cell 3 11,growx,aligny top");

		lblNewLabel_1.setForeground(new Color(204, 204, 204));
		lblNewLabel_1.setFont(new Font("Bookman Old Style", Font.BOLD, 12));
		add(lblNewLabel_1, "cell 0 12,alignx trailing");

		textField_1 = new JTextField();
		textField_1.setFont(new Font("Bookman Old Style", Font.BOLD, 15));
		textField_1.setBackground(new Color(204, 204, 204));
		textField_1.setColumns(10);
		add(textField_1, "cell 1 12,growx,aligny top");
		textField_9 = new JTextField();
		textField_9.setFont(new Font("Bookman Old Style", Font.BOLD, 15));
		textField_9.setBackground(new Color(204, 204, 204));
		textField_9.setColumns(10);
		add(textField_9, "cell 3 12,growx,aligny top");

		lblNewLabel_8.setForeground(new Color(204, 204, 204));
		lblNewLabel_8.setFont(new Font("Bookman Old Style", Font.BOLD, 12));
		add(lblNewLabel_8, "cell 4 12");

		JLabel lblNazwisko1 = new JLabel("Nazwisko");
		lblNazwisko1.setForeground(new Color(204, 204, 204));
		lblNazwisko1.setFont(new Font("Bookman Old Style", Font.ITALIC, 18));
		add(lblNazwisko1, "cell 1 13,growx,aligny top");

		JLabel label_7 = new JLabel("Telefon do zak�adu pracy");
		label_7.setForeground(new Color(204, 204, 204));
		label_7.setFont(new Font("Bookman Old Style", Font.ITALIC, 18));
		add(label_7, "cell 3 13,growx,aligny top");

		lblNewLabel_2.setForeground(new Color(204, 204, 204));
		lblNewLabel_2.setFont(new Font("Bookman Old Style", Font.BOLD, 12));
		add(lblNewLabel_2, "cell 0 14,alignx trailing");

		textField_2 = new JTextField();
		textField_2.setFont(new Font("Bookman Old Style", Font.BOLD, 15));
		textField_2.setBackground(new Color(204, 204, 204));
		textField_2.setColumns(10);
		add(textField_2, "cell 1 14,growx,aligny top");
		textField_10 = new JTextField();
		textField_10.setFont(new Font("Bookman Old Style", Font.BOLD, 15));
		textField_10.setBackground(new Color(204, 204, 204));
		textField_10.setColumns(10);
		add(textField_10, "cell 3 14,growx,aligny top");

		lblNewLabel_9.setForeground(new Color(204, 204, 204));
		lblNewLabel_9.setFont(new Font("Bookman Old Style", Font.BOLD, 12));
		add(lblNewLabel_9, "cell 4 14");

		JLabel label = new JLabel("Adres zamieszkania");
		label.setForeground(new Color(204, 204, 204));
		label.setFont(new Font("Bookman Old Style", Font.ITALIC, 18));
		add(label, "cell 1 15,growx,aligny top");

		JLabel label_8 = new JLabel("Stanowisko");
		label_8.setForeground(new Color(204, 204, 204));
		label_8.setFont(new Font("Bookman Old Style", Font.ITALIC, 18));
		add(label_8, "cell 3 15,growx,aligny top");

		lblNewLabel_3.setForeground(new Color(204, 204, 204));
		lblNewLabel_3.setFont(new Font("Bookman Old Style", Font.BOLD, 12));
		add(lblNewLabel_3, "cell 0 16,alignx trailing");

		textField_3 = new JTextField();
		textField_3.setFont(new Font("Bookman Old Style", Font.BOLD, 15));
		textField_3.setBackground(new Color(204, 204, 204));
		textField_3.setColumns(10);
		add(textField_3, "cell 1 16,growx,aligny top");
		textField_11 = new JTextField();
		textField_11.setFont(new Font("Bookman Old Style", Font.BOLD, 15));
		textField_11.setBackground(new Color(204, 204, 204));
		textField_11.setColumns(10);
		add(textField_11, "cell 3 16,growx,aligny top");

		lblNewLabel_10.setForeground(new Color(204, 204, 204));
		lblNewLabel_10.setFont(new Font("Bookman Old Style", Font.BOLD, 12));
		add(lblNewLabel_10, "cell 4 16");

		JLabel label_1 = new JLabel("Numer dowodu");
		label_1.setForeground(new Color(204, 204, 204));
		label_1.setFont(new Font("Bookman Old Style", Font.ITALIC, 18));
		add(label_1, "cell 1 17,growx,aligny top");

		JLabel label_9 = new JLabel("Zarobki miesi�czne NETTO");
		label_9.setForeground(new Color(204, 204, 204));
		label_9.setFont(new Font("Bookman Old Style", Font.ITALIC, 18));
		add(label_9, "cell 3 17,growx,aligny top");

		lblNewLabel_4.setForeground(new Color(204, 204, 204));
		lblNewLabel_4.setFont(new Font("Bookman Old Style", Font.BOLD, 12));
		add(lblNewLabel_4, "cell 0 18,alignx trailing");

		textField_4 = new JTextField();
		textField_4.setFont(new Font("Bookman Old Style", Font.BOLD, 15));
		textField_4.setBackground(new Color(204, 204, 204));
		textField_4.setColumns(10);
		add(textField_4, "cell 1 18,growx,aligny top");
		textField_12 = new JTextField();
		textField_12.setFont(new Font("Bookman Old Style", Font.BOLD, 15));
		textField_12.setBackground(new Color(204, 204, 204));
		textField_12.setColumns(10);
		add(textField_12, "cell 3 18,growx,aligny top");

		lblNewLabel_11.setForeground(new Color(204, 204, 204));
		lblNewLabel_11.setFont(new Font("Bookman Old Style", Font.BOLD, 12));
		add(lblNewLabel_11, "cell 4 18");

		JLabel label_2 = new JLabel("Telefon");
		label_2.setForeground(new Color(204, 204, 204));
		label_2.setFont(new Font("Bookman Old Style", Font.ITALIC, 18));
		add(label_2, "cell 1 19,growx,aligny top");

		JLabel label_10 = new JLabel("Okres umowy");
		label_10.setForeground(new Color(204, 204, 204));
		label_10.setFont(new Font("Bookman Old Style", Font.ITALIC, 18));
		add(label_10, "cell 3 19,growx,aligny top");

		lblNewLabel_5.setForeground(new Color(204, 204, 204));
		lblNewLabel_5.setFont(new Font("Bookman Old Style", Font.BOLD, 12));
		add(lblNewLabel_5, "cell 0 20,alignx trailing");

		textField_5 = new JTextField();
		textField_5.setFont(new Font("Bookman Old Style", Font.BOLD, 15));
		textField_5.setBackground(new Color(204, 204, 204));
		textField_5.setColumns(10);
		add(textField_5, "cell 1 20,growx,aligny top");
		textField_13 = new JTextField();
		textField_13.setFont(new Font("Bookman Old Style", Font.BOLD, 15));
		textField_13.setBackground(new Color(204, 204, 204));
		textField_13.setColumns(10);
		add(textField_13, "cell 3 20,growx,aligny top");

		lblNewLabel_12.setForeground(new Color(204, 204, 204));
		lblNewLabel_12.setFont(new Font("Bookman Old Style", Font.BOLD, 12));
		add(lblNewLabel_12, "cell 4 20");

		JLabel label_3 = new JLabel("Adres E-MAIL");
		label_3.setForeground(new Color(204, 204, 204));
		label_3.setFont(new Font("Bookman Old Style", Font.ITALIC, 18));
		add(label_3, "cell 1 21,growx,aligny top");

		JLabel label_11 = new JLabel("Doch�d dodatkowy");
		label_11.setForeground(new Color(204, 204, 204));
		label_11.setFont(new Font("Bookman Old Style", Font.ITALIC, 18));
		add(label_11, "cell 3 21,growx,aligny top");

		lblNewLabel_6.setForeground(new Color(204, 204, 204));
		lblNewLabel_6.setFont(new Font("Bookman Old Style", Font.BOLD, 12));
		add(lblNewLabel_6, "cell 0 22,alignx trailing");

		textField_6 = new JTextField();
		textField_6.setFont(new Font("Bookman Old Style", Font.BOLD, 15));
		textField_6.setBackground(new Color(204, 204, 204));
		textField_6.setColumns(10);
		add(textField_6, "cell 1 22,growx,aligny top");
		textField_14 = new JTextField();
		textField_14.setFont(new Font("Bookman Old Style", Font.BOLD, 15));
		textField_14.setBackground(new Color(204, 204, 204));
		textField_14.setColumns(10);
		add(textField_14, "cell 3 22,growx,aligny top");

		lblNewLabel_13.setForeground(new Color(204, 204, 204));
		lblNewLabel_13.setFont(new Font("Bookman Old Style", Font.BOLD, 12));
		add(lblNewLabel_13, "cell 4 22");

		JLabel label_4 = new JLabel("Ilo�� os�b na utrzymaniu");
		label_4.setForeground(new Color(204, 204, 204));
		label_4.setFont(new Font("Bookman Old Style", Font.ITALIC, 18));
		add(label_4, "cell 1 23 2 1,growx,aligny top");

		JLabel label_12 = new JLabel("Koszta dodatkowe");
		label_12.setForeground(new Color(204, 204, 204));
		label_12.setFont(new Font("Bookman Old Style", Font.ITALIC, 18));
		add(label_12, "cell 3 23,growx,aligny top");

		lblNewLabel_7.setForeground(new Color(204, 204, 204));
		lblNewLabel_7.setFont(new Font("Bookman Old Style", Font.BOLD, 12));
		add(lblNewLabel_7, "cell 0 24,alignx trailing");

		textField_7 = new JTextField();
		textField_7.setFont(new Font("Bookman Old Style", Font.BOLD, 15));
		textField_7.setBackground(new Color(204, 204, 204));
		textField_7.setColumns(10);
		add(textField_7, "cell 1 24,growx,aligny top");
		textField_15 = new JTextField();
		textField_15.setFont(new Font("Bookman Old Style", Font.BOLD, 15));
		textField_15.setBackground(new Color(204, 204, 204));
		textField_15.setColumns(10);
		add(textField_15, "cell 3 24,growx,aligny top");

		lblNewLabel_14.setForeground(new Color(204, 204, 204));
		lblNewLabel_14.setFont(new Font("Bookman Old Style", Font.BOLD, 12));
		add(lblNewLabel_14, "cell 4 24");

		JLabel label_16 = new JLabel("DANE KREDYTU");
		label_16.setForeground(new Color(204, 204, 204));
		label_16.setHorizontalAlignment(SwingConstants.CENTER);
		label_16.setFont(new Font("Bookman Old Style", Font.BOLD | Font.ITALIC, 25));
		add(label_16, "cell 1 28 3 1,alignx center,aligny top");

		// STRONA LEWA
		JLabel lblPesel11 = new JLabel("Sklep");
		lblPesel11.setForeground(new Color(204, 204, 204));
		lblPesel11.setFont(new Font("Bookman Old Style", Font.ITALIC, 18));
		add(lblPesel11, "cell 1 32,growx,aligny top");

		// STRONA PRAWA
		JLabel label_51 = new JLabel("Wp�ata w�asna");
		label_51.setForeground(new Color(204, 204, 204));
		label_51.setFont(new Font("Bookman Old Style", Font.ITALIC, 18));
		add(label_51, "cell 3 32,growx,aligny top");

		lblNewLabel_15.setForeground(new Color(204, 204, 204));
		lblNewLabel_15.setFont(new Font("Bookman Old Style", Font.BOLD, 12));
		add(lblNewLabel_15, "cell 0 33,alignx trailing");

		textField_16 = new JTextField();
		textField_16.setFont(new Font("Bookman Old Style", Font.BOLD, 15));
		textField_16.setBackground(new Color(204, 204, 204));
		textField_16.setColumns(10);
		add(textField_16, "cell 1 33,growx,aligny top");
		textField_20 = new JTextField();
		textField_20.setFont(new Font("Bookman Old Style", Font.BOLD, 15));
		textField_20.setBackground(new Color(204, 204, 204));
		textField_20.setColumns(10);
		add(textField_20, "cell 3 33,growx,aligny top");

		lblNewLabel_19.setForeground(new Color(204, 204, 204));
		lblNewLabel_19.setFont(new Font("Bookman Old Style", Font.BOLD, 12));
		add(lblNewLabel_19, "cell 4 33");

		JLabel lblImie1 = new JLabel("Sprz�t");
		lblImie1.setForeground(new Color(204, 204, 204));
		lblImie1.setFont(new Font("Bookman Old Style", Font.ITALIC, 18));
		add(lblImie1, "cell 1 34,growx,aligny top");

		JLabel label_61 = new JLabel("Ubezpieczenie");
		label_61.setForeground(new Color(204, 204, 204));
		label_61.setFont(new Font("Bookman Old Style", Font.ITALIC, 18));
		add(label_61, "cell 3 34,growx,aligny top");

		lblNewLabel_16.setForeground(new Color(204, 204, 204));
		lblNewLabel_16.setFont(new Font("Bookman Old Style", Font.BOLD, 12));
		add(lblNewLabel_16, "cell 0 35,alignx trailing");

		textField_17 = new JTextField();
		textField_17.setFont(new Font("Bookman Old Style", Font.BOLD, 15));
		textField_17.setBackground(new Color(204, 204, 204));
		textField_17.setColumns(10);
		add(textField_17, "cell 1 35,growx,aligny top");

		comboBox = new JComboBox<String>();
		comboBox.setBackground(new Color(204, 204, 204));
		comboBox.setFont(new Font("Bookman Old Style", Font.BOLD, 15));
		comboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String text = comboBox.getSelectedItem().toString();
				if (text.equals("Brak ubezpieczenia"))
					ubezpieczenie = 0;
				if (text.equals("1 %"))
					ubezpieczenie = 1;
				if (text.equals("5 %"))
					ubezpieczenie = 5;
				if (text.equals("10 %"))
					ubezpieczenie = 10;
				if (text.equals("20 %"))
					ubezpieczenie = 20;
			}
		});
		comboBox.addItem("Brak ubezpieczenia");
		comboBox.addItem("1 %");
		comboBox.addItem("5 %");
		comboBox.addItem("10 %");
		comboBox.addItem("20 %");
		add(comboBox, "cell 3 35,growx");

		JLabel lblNazwisko11 = new JLabel("Model");
		lblNazwisko11.setForeground(new Color(204, 204, 204));
		lblNazwisko11.setFont(new Font("Bookman Old Style", Font.ITALIC, 18));
		add(lblNazwisko11, "cell 1 36,growx,aligny top");

		JLabel label_71 = new JLabel("Ilo�� rat");
		label_71.setForeground(new Color(204, 204, 204));
		label_71.setFont(new Font("Bookman Old Style", Font.ITALIC, 18));
		add(label_71, "cell 3 36,growx,aligny top");

		lblNewLabel_17.setForeground(new Color(204, 204, 204));
		lblNewLabel_17.setFont(new Font("Bookman Old Style", Font.BOLD, 12));
		add(lblNewLabel_17, "cell 0 37,alignx trailing");

		textField_18 = new JTextField();
		textField_18.setFont(new Font("Bookman Old Style", Font.BOLD, 15));
		textField_18.setBackground(new Color(204, 204, 204));
		textField_18.setColumns(10);
		add(textField_18, "cell 1 37,growx,aligny top");

		textField_22 = new JTextField();
		textField_22.setFont(new Font("Bookman Old Style", Font.BOLD, 15));
		textField_22.setBackground(new Color(204, 204, 204));
		textField_22.setColumns(10);
		add(textField_22, "cell 3 37,growx,aligny top");

		lblNewLabel_20.setForeground(new Color(204, 204, 204));
		lblNewLabel_20.setFont(new Font("Bookman Old Style", Font.BOLD, 12));
		add(lblNewLabel_20, "cell 4 37");

		JLabel label_13 = new JLabel("Cena");
		label_13.setForeground(new Color(204, 204, 204));
		label_13.setFont(new Font("Bookman Old Style", Font.ITALIC, 18));
		add(label_13, "cell 1 38,growx,aligny top");

		JLabel label_81 = new JLabel("Promocja");
		label_81.setForeground(new Color(204, 204, 204));
		label_81.setFont(new Font("Bookman Old Style", Font.ITALIC, 18));
		add(label_81, "cell 3 38,growx,aligny top");

		lblNewLabel_18.setForeground(new Color(204, 204, 204));
		lblNewLabel_18.setFont(new Font("Bookman Old Style", Font.BOLD, 12));
		add(lblNewLabel_18, "cell 0 39,alignx trailing");

		textField_19 = new JTextField();
		textField_19.setFont(new Font("Bookman Old Style", Font.BOLD, 15));
		textField_19.setBackground(new Color(204, 204, 204));
		textField_19.setColumns(10);
		add(textField_19, "cell 1 39,growx,aligny top");

		// PRZYCISKI DODATKOWE

		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		Date date = new Date();
		Random generator = new Random();
		JButton btnOblicz1 = new JButton("WY\u015ALIJ WNIOSEK");
		btnOblicz1.setForeground(new Color(0, 0, 0));
		btnOblicz1.setBackground(new Color(102, 102, 102));
		btnOblicz1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int id = generator.nextInt(2000);
				czysc();
				sprawdz();
				String[] wniosek = { String.valueOf(id), textField_2.getText(), textField_1.getText(),
						textField.getText(), dateFormat.format(date), textField_3.getText(), textField_4.getText(),
						textField_5.getText(), textField_6.getText(), textField_7.getText(), textField_8.getText(),
						textField_9.getText(), textField_10.getText(), textField_11.getText(), textField_12.getText(),
						textField_13.getText(), textField_14.getText(), textField_15.getText(), textField_16.getText(),
						textField_17.getText(), textField_18.getText(), textField_19.getText(), textField_20.getText(),
						String.valueOf(ubezpieczenie), textField_22.getText(), String.valueOf(promocja) };
				BazaDanych.dodajWniosek(wniosek);
			}
		});

		comboBox_1 = new JComboBox<String>();
		comboBox_1.setBackground(new Color(204, 204, 204));
		comboBox_1.setFont(new Font("Bookman Old Style", Font.BOLD, 15));
		comboBox_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String text = comboBox_1.getSelectedItem().toString();
				if (text.equals("Standard"))
					promocja = 0.02;
				if (text.equals("1,5 %"))
					promocja = 0.015;
				if (text.equals("1 %"))
					promocja = 0.01;
				if (text.equals("0,5 %"))
					promocja = 0.005;
			}
		});
		comboBox_1.addItem("Standard");
		comboBox_1.addItem("1,5 %");
		comboBox_1.addItem("1 %");
		comboBox_1.addItem("0,5 %");
		add(comboBox_1, "cell 3 39,growx");

		btnOblicz1.setFont(new Font("Bookman Old Style", Font.BOLD | Font.ITALIC, 23));
		add(btnOblicz1, "cell 3 47,alignx right,aligny top");

	}

	public void sprawdz() {

		if (textField.getText().isEmpty()) // pesel
			label_15.setText("Brak danych!");
		else if (textField.getText().length() != 11)
			label_15.setText("Niepoprawna d�ugo��!");

		if (textField_1.getText().isEmpty()) // imie
			lblNewLabel_1.setText("Brak danych!");
		if (textField_2.getText().isEmpty()) // nazwisko
			lblNewLabel_2.setText("Brak danych!");
		if (textField_3.getText().isEmpty()) // adres
			lblNewLabel_3.setText("Brak danych!");
		if (textField_4.getText().isEmpty()) // dowod
			lblNewLabel_4.setText("Brak danych!");
		else if (textField_4.getText().length() != 9)
			lblNewLabel_4.setText("Niepoprawna d�ugo��!");

		if (textField_5.getText().isEmpty()) // telefon
			lblNewLabel_5.setText("Brak danych!");
		else if (!textField_5.getText().isEmpty()) {
			if (textField_5.getText().length() != 9)
				lblNewLabel_5.setText("Niepoprawna d�ugo��!");
			try {
				int telefonKlient = Integer.parseInt(textField_5.getText());
			} catch (NumberFormatException e) {
				lblNewLabel_5.setText("Niepoprawne dane!");
			}
		}

		if (textField_6.getText().isEmpty()) // @
			lblNewLabel_6.setText("Brak danych!");
		if (textField_7.getText().isEmpty()) {// osoby
			lblNewLabel_7.setText("Brak danych!");
		}
		if (!textField_7.getText().isEmpty()) {
			try {
				int osoby = Integer.parseInt(textField_7.getText());
			} catch (NumberFormatException e) {
				lblNewLabel_7.setText("Niepoprawne dane!");
			}
		}

		if (textField_8.getText().isEmpty()) // praca
			lblNewLabel.setText("Brak danych!");
		if (textField_9.getText().isEmpty()) // zatridnienie
			lblNewLabel_8.setText("Brak danych!");
		if (textField_10.getText().isEmpty()) // telefon
			lblNewLabel_9.setText("Brak danych!");
		else if (!textField_10.getText().isEmpty()) {
			if (textField_10.getText().length() != 9)
				lblNewLabel_9.setText("Niepoprawna d�ugo��!");
			try {
				int telefonPraca = Integer.parseInt(textField_10.getText());
			} catch (NumberFormatException e) {
				lblNewLabel_9.setText("Niepoprawne dane!");
			}
		}

		if (textField_11.getText().isEmpty()) // stanowisko
			lblNewLabel_10.setText("Brak danych!");
		if (textField_12.getText().isEmpty()) // zarobki
			lblNewLabel_11.setText("Brak danych!");
		else if (!textField_12.getText().isEmpty()) {
			try {
				double zarobki = Double.parseDouble(textField_12.getText());
			} catch (NumberFormatException e) {
				lblNewLabel_11.setText("Niepoprawne dane!");
			}
		}

		if (textField_13.getText().isEmpty()) // okres
			lblNewLabel_12.setText("Brak danych!");
		if (textField_14.getText().isEmpty()) // zarobkiplus
			lblNewLabel_13.setText("Brak danych!");
		else if (!textField_14.getText().isEmpty()) {
			if (textField_14.getText().length() > 5)
				lblNewLabel_13.setText("Niepoprawna d�ugo��!");
			try {
				double zarobkiPlus = Double.parseDouble(textField_14.getText());
			} catch (NumberFormatException e) {
				lblNewLabel_13.setText("Niepoprawne dane!");
			}
		}

		if (textField_15.getText().isEmpty()) // kosztaplus
			lblNewLabel_14.setText("Brak danych!");
		else if (!textField_15.getText().isEmpty()) {
			if (textField_15.getText().length() > 5)
				lblNewLabel_14.setText("Niepoprawna d�ugo��!");
			try {
				double kosztaPlus = Double.parseDouble(textField_15.getText());
			} catch (NumberFormatException e) {
				lblNewLabel_14.setText("Niepoprawne dane!");
			}
		}

		if (textField_16.getText().isEmpty()) // sklep
			lblNewLabel_15.setText("Brak danych!");
		if (textField_17.getText().isEmpty()) // sprzet
			lblNewLabel_16.setText("Brak danych!");
		if (textField_18.getText().isEmpty()) // model
			lblNewLabel_17.setText("Brak danych!");
		if (textField_19.getText().isEmpty()) // cena
			lblNewLabel_18.setText("Brak danych!");
		else if (!textField_19.getText().isEmpty()) {
			if (textField_19.getText().length() > 5)
				lblNewLabel_14.setText("Niepoprawna d�ugo��!");
			try {
				double cena = Double.parseDouble(textField_19.getText());
			} catch (NumberFormatException e) {
				lblNewLabel_18.setText("Niepoprawne dane!");
			}
		}

		if (textField_20.getText().isEmpty()) // wplata
			lblNewLabel_19.setText("Brak danych!");
		else if (!textField_20.getText().isEmpty()) {
			if (textField_20.getText().length() > 5)
				lblNewLabel_19.setText("Niepoprawna d�ugo��!");
			try {
				double wplata = Double.parseDouble(textField_20.getText());
			} catch (NumberFormatException e) {
				lblNewLabel_19.setText("Niepoprawne dane!");
			}
		}

		if (textField_22.getText().isEmpty()) // raty
			lblNewLabel_20.setText("Brak danych!");
		else if (!textField_22.getText().isEmpty()) {
			if (textField_22.getText().length() > 3)
				lblNewLabel_20.setText("Niepoprawna d�ugo��!");
			try {
				int raty = Integer.parseInt(textField_22.getText());
			} catch (NumberFormatException e) {
				lblNewLabel_20.setText("Niepoprawne dane!");
			}
		}
	}

	public static void czysc() {
		label_15.setText("");
		lblNewLabel.setText("");
		lblNewLabel_1.setText("");
		lblNewLabel_2.setText("");
		lblNewLabel_3.setText("");
		lblNewLabel_4.setText("");
		lblNewLabel_5.setText("");
		lblNewLabel_6.setText("");
		lblNewLabel_7.setText("");
		lblNewLabel_8.setText("");
		lblNewLabel_9.setText("");
		lblNewLabel_10.setText("");
		lblNewLabel_11.setText("");
		lblNewLabel_12.setText("");
		lblNewLabel_13.setText("");
		lblNewLabel_14.setText("");
		lblNewLabel_15.setText("");
		lblNewLabel_16.setText("");
		lblNewLabel_17.setText("");
		lblNewLabel_18.setText("");
		lblNewLabel_19.setText("");
		lblNewLabel_20.setText("");

	}
}
