package GUI;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import javax.swing.JMenuBar;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.awt.event.ActionEvent;
import net.miginfocom.swing.MigLayout;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import BAZA.DBMySQL;
import TCP.BazaDanych;

import javax.swing.SwingConstants;
import java.awt.Color;
import java.awt.Font;

public class AplikacjaKredytowa extends Thread {

	SymulacjaPanel symulacja;
	WniosekPanel wniosek;
	ListaPanel list;
	static JFrame frame;
	static JPanel panel;
	static JMenuBar menuBar;
	static JScrollPane scrollPane;
	
	static String[][] lista;
	static int numOfRows;
	static TableColumn tc;
	static String[] Q;

	static Thread serwer, client;
	static Socket socket;
	static BufferedReader inFromServer;
	static PrintWriter outToServer;

	/**
	 * Launch the application.
	 */
	public void run() {
		try {
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
				| UnsupportedLookAndFeelException e1) {
			e1.printStackTrace();
		}
		try {
			symulacja = new SymulacjaPanel();
			wniosek = new WniosekPanel();
			list = new ListaPanel();
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
				| UnsupportedLookAndFeelException e1) {
			e1.printStackTrace();
		}

		scrollPane = new JScrollPane(wniosek);

		frame = new JFrame("Klient");
		frame.setBackground(new Color(25, 25, 25));
		frame.setResizable(true);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setBounds(300, 10, 1000, 800);
		frame.getContentPane().setLayout(new BorderLayout(0, 0));
		panel = new JPanel();
		panel.setBackground(new Color(25, 25, 25));
		panel.setBounds(0, 0, 1920, 1080);
		frame.getContentPane().add(panel);
		panel.setLayout(new MigLayout("", "[]", "[]"));
		frame.remove(scrollPane);
		frame.remove(panel);
		panel = symulacja;
		frame.getContentPane().add(panel);
		frame.repaint();
		frame.revalidate();

		JMenuBar menuBar_1 = new JMenuBar();
		menuBar_1.setBackground(new Color(25, 25, 25));
		frame.setJMenuBar(menuBar_1);

		JButton btnSymulacjaKredytowa = new JButton("Symulacja wniosku");
		btnSymulacjaKredytowa.setFont(new Font("Bookman Old Style", Font.BOLD, 15));
		btnSymulacjaKredytowa.setForeground(new Color(204, 204, 204));
		btnSymulacjaKredytowa.setBackground(new Color(25, 25, 25));
		btnSymulacjaKredytowa.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					symulacja = new SymulacjaPanel();
				} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
						| UnsupportedLookAndFeelException e1) {
					e1.printStackTrace();
				}
				frame.remove(scrollPane);
				frame.remove(panel);
				panel = symulacja;
				frame.getContentPane().add(panel);
				frame.repaint();
				frame.revalidate();
			}
		});
		menuBar_1.add(btnSymulacjaKredytowa);

		JButton btnWniosekKredytowy = new JButton("Wniosek kredytowy");
		btnWniosekKredytowy.setFont(new Font("Bookman Old Style", Font.BOLD, 15));
		btnWniosekKredytowy.setBackground(new Color(25, 25, 25));
		btnWniosekKredytowy.setForeground(new Color(204, 204, 204));
		btnWniosekKredytowy.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.remove(scrollPane);
				frame.remove(panel);
				try {
					scrollPane = new JScrollPane(new WniosekPanel());
				} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
						| UnsupportedLookAndFeelException e1) {
					e1.printStackTrace();
				}
				scrollPane.setPreferredSize(new Dimension(5000, 5000));
				frame.getContentPane().add(scrollPane);
				frame.repaint();
				frame.revalidate();
				WniosekPanel.czysc();
			}
		});
		menuBar_1.add(btnWniosekKredytowy);

		JButton btnListaWnioskw = new JButton("Lista wniosk\u00F3w");
		btnListaWnioskw.setFont(new Font("Bookman Old Style", Font.BOLD, 15));
		btnListaWnioskw.setBackground(new Color(25, 25, 25));
		btnListaWnioskw.setForeground(new Color(204, 204, 204));
		btnListaWnioskw.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					getNumOfRows();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				frame.remove(scrollPane);
				frame.remove(panel);
				panel = list;
				frame.getContentPane().add(panel);
				frame.repaint();
				frame.revalidate();
			}
		});
		menuBar_1.add(btnListaWnioskw);

		JButton btnWyloguj = new JButton("WYLOGUJ");
		btnWyloguj.setFont(new Font("Bookman Old Style", Font.BOLD, 15));
		btnWyloguj.setBackground(new Color(25, 25, 25));
		btnWyloguj.setForeground(new Color(204, 204, 204));
		btnWyloguj.setHorizontalAlignment(SwingConstants.LEADING);
		btnWyloguj.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				closeConnetction();
				frame.dispose();
				Client.frame.setVisible(true);
			}
		});
		menuBar_1.add(btnWyloguj);

		frame.setVisible(true);
	}

	static void connect(String login, String haslo) {
		try {
			if (DBMySQL.checkUser(login, haslo)) {
				socket = new Socket(InetAddress.getByName("127.0.0.1"), 123);
				inFromServer = new BufferedReader(new InputStreamReader(socket.getInputStream()));
				outToServer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));
				client = new AplikacjaKredytowa();
				client.start();
				Thread BD = new BazaDanych();
				BD.start();
				Client.textField.setText("");
				Client.passwordField.setText("");
				Client.lblNewLabel.setText("");
				outToServer.println("niu" + login);
				outToServer.flush();
				Client.close();
			} else {
				Client.lblNewLabel.setText("Z�e dane!");
			}
		} catch (Exception e) {
			Client.lblNewLabel.setText("Brak po��czenia z bankiem!");
		}
	}

	static int getNumOfRows() throws IOException {
		outToServer.println("iloscwierszy");
		outToServer.flush();

		numOfRows = inFromServer.read();
		System.out.println("Ilosc wierszy " + numOfRows);
		if (numOfRows != 0)
			getList(numOfRows);
		else {
			ListaPanel.table.setModel(new DefaultTableModel(ListaPanel.columnNames, 1));
			for (int i = 0; i < 7; i++) {
				TableCellRenderer rendererFromHeader = ListaPanel.table.getTableHeader().getDefaultRenderer();
				JLabel headerLabel = (JLabel) rendererFromHeader;
				headerLabel.setHorizontalAlignment(JLabel.CENTER);

				TableColumn tc = ListaPanel.table.getColumnModel().getColumn(i);
				tc.setHeaderValue(ListaPanel.columnNames[i]);
				DefaultTableCellRenderer dtcr = new DefaultTableCellRenderer();
				dtcr.setHorizontalAlignment(SwingConstants.CENTER);
				ListaPanel.table.setRowHeight(30);
				tc.setCellRenderer(dtcr);
			}
		}
		return numOfRows;
	}

	static void getList(int numOfRows) throws IOException {
		outToServer.println("lista");
		outToServer.flush();

		lista = new String[numOfRows][7];
		int nr = 0;
		String check;
		while ((check = inFromServer.readLine()) != null) {
			lista[nr / 7][nr % 7] = check;
			nr++;
			if (nr == (numOfRows * 7))
				break;
		}

		ListaPanel.table.setModel(new DefaultTableModel(ListaPanel.columnNames, numOfRows));

		for (int i = 0; i < numOfRows; i++) {
			for (int j = 0; j <= 6; j++) {
				TableCellRenderer rendererFromHeader = ListaPanel.table.getTableHeader().getDefaultRenderer();
				JLabel headerLabel = (JLabel) rendererFromHeader;
				headerLabel.setHorizontalAlignment(JLabel.CENTER);

				TableColumn tc = ListaPanel.table.getColumnModel().getColumn(j);
				tc.setHeaderValue(ListaPanel.columnNames[j]);
				DefaultTableCellRenderer dtcr = new DefaultTableCellRenderer();
				dtcr.setHorizontalAlignment(SwingConstants.CENTER);
				ListaPanel.table.setRowHeight(30);
				tc.setCellRenderer(dtcr);

				String text = lista[i][j];
				ListaPanel.table.setValueAt(text, i, j);
//				ListaPanel.table.getModel().isCellEditable(i, 6);
			}
		}

		Action accept = new AbstractAction() {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent e) {
				JTable table = (JTable) e.getSource();
				int modelRow = Integer.valueOf(e.getActionCommand());
				String id_wniosku = table.getModel().getValueAt(modelRow, 0).toString();
				if ((table.getModel().getValueAt(modelRow, 5).toString()).equals("Zaakceptowano")) {
					((DefaultTableModel) table.getModel()).setValueAt("Zatwierdzono", modelRow, 5);
					BazaDanych.changeStatus(id_wniosku);
				} else if ((table.getModel().getValueAt(modelRow, 5).toString()).equals("Odrzucono")) {
					((DefaultTableModel) table.getModel()).removeRow(modelRow);
					BazaDanych.delete(id_wniosku);
				} else if ((table.getModel().getValueAt(modelRow, 5).toString()).equals("Oczekiwanie")) {
					((DefaultTableModel) table.getModel()).setValueAt("Zatwierd�", modelRow, 6);
					BazaDanych.changeButton(id_wniosku);
					Q = new String[9];
					Q = BazaDanych.pobierzDane(Integer.parseInt(id_wniosku), Q);
					try {
						double kwotaRaty = obliczKRW(Double.parseDouble(Q[4]), Integer.parseInt(Q[5]),
								Double.parseDouble(Q[6]), Double.parseDouble(Q[7]), Integer.parseInt(Q[8]));
						
						String zgoda = checkStatus(Integer.parseInt(id_wniosku), Double.parseDouble(Q[0]), kwotaRaty,
								Double.parseDouble(Q[1]), Double.parseDouble(Q[2]), Integer.parseInt(Q[3]));
						
						((DefaultTableModel) table.getModel()).setValueAt(zgoda, modelRow, 5);
						
					} catch (NumberFormatException | IOException e1) {
						e1.printStackTrace();
					}
				}
			}
		};
		ButtonColumn buttonColumn = new ButtonColumn(ListaPanel.table, accept, 6);
		buttonColumn.setMnemonic(KeyEvent.VK_D);
	}

	static void obliczKR(double kwota, double wplata, int lR, double promocja, int ubezpieczenie) throws IOException{
		outToServer.println("ObliczKwoteRat");
		outToServer.println(kwota);
		outToServer.println(wplata);
		outToServer.println(lR);
		outToServer.println(promocja);
		outToServer.println(ubezpieczenie);
		outToServer.flush();
		
		String from=inFromServer.readLine();
		SymulacjaPanel.kwotaRaty=Double.parseDouble(from);
	}
	
	static void obliczLR(double kwota, double wplata, double kR, double promocja, int ubezpieczenie) throws IOException{
		outToServer.println("ObliczLiczbeRat");
		outToServer.println(kwota);
		outToServer.println(wplata);
		outToServer.println(kR);
		outToServer.println(promocja);
		outToServer.println(ubezpieczenie);
		outToServer.flush();
		
		String from=inFromServer.readLine();
		SymulacjaPanel.liczbaRat=Integer.parseInt(from);
	}
	
	static double obliczKRW(double kwota,int lR, double wplata, double promocja, int ubezpieczenie) throws IOException{
		outToServer.println("ObliczKwoteRatWniosek");
		outToServer.println(kwota);
		outToServer.println(wplata);
		outToServer.println(lR);
		outToServer.println(promocja);
		outToServer.println(ubezpieczenie);
		outToServer.flush();
		
		String from=inFromServer.readLine();
		return Double.parseDouble(from);
	}
	
	static String checkStatus(int id, double dochod,double kwotaRaty,double dochodDodatkowy,double koszta,int iloscOsob) throws IOException{
		outToServer.println("SprawdzStatus");
		outToServer.println(id);
		outToServer.println(dochod);
		outToServer.println(kwotaRaty);
		outToServer.println(dochodDodatkowy);
		outToServer.println(koszta);
		outToServer.println(iloscOsob);
		outToServer.flush();
		
		String status=inFromServer.readLine();
		return status;
	}
	
	static void closeConnetction() {
		outToServer.println("exit");
		outToServer.flush();
	}
}
