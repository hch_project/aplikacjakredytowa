package GUI;

import java.awt.Font;
import java.awt.event.ActionEvent;

import net.miginfocom.swing.MigLayout;

import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.*;

import java.awt.Color;

public class SymulacjaPanel extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static JTextField textField;
	public static JTextField textField_1;
	public static JTextField textField_4;
	public static JTextField textField_5;
	public static JLabel label_1;
	public static JComboBox<String> comboBox, comboBox_1;
	public static JProgressBar progressBar;
	private int ubezpieczenie;
	private double promocja;
	static int liczbaRat;
	static Double kwotaRaty;
	public static Thread oIR, oKR;
	/**
	 * Create the panel.
	 * 
	 * @throws UnsupportedLookAndFeelException
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 * @throws ClassNotFoundException
	 */
	public SymulacjaPanel() throws ClassNotFoundException, InstantiationException, IllegalAccessException,
			UnsupportedLookAndFeelException {
		setBackground(new Color(25, 25, 25));

		UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
		setBounds(0, 0, 960, 870);
		setLayout(new MigLayout("", "[146px,grow][220.00px][99.00px][220.00px][123px,grow]", "[59.00][50px][24px][20px][24px][20px][24px][][31px][20px][18px][39px][13.00][][27.00][][]"));
		
		JLabel label = new JLabel("");
		add(label, "cell 2 0");

		// setLayout(null);

		JLabel lblSymulacjaWniosku = new JLabel("SYMULACJA WNIOSKU");
		lblSymulacjaWniosku.setForeground(new Color(204, 204, 204));
		lblSymulacjaWniosku.setFont(new Font("Bookman Old Style", Font.BOLD | Font.ITALIC, 40));
		lblSymulacjaWniosku.setHorizontalAlignment(SwingConstants.CENTER);
		add(lblSymulacjaWniosku, "cell 1 1 3 1,growx,aligny top");

		JLabel lblKwotaKredytu = new JLabel("Kwota kredytu");
		lblKwotaKredytu.setForeground(new Color(204, 204, 204));
		lblKwotaKredytu.setFont(new Font("Bookman Old Style", Font.ITALIC, 18));
		add(lblKwotaKredytu, "cell 1 3,growx,aligny top");

		textField = new JTextField();
		textField.setBackground(new Color(204, 204, 204));
		textField.setFont(new Font("Bookman Old Style", Font.BOLD, 15));
		textField.setColumns(10);
		add(textField, "cell 1 4,grow");

		JLabel lblWpataWasna = new JLabel("Wp\u0142ata w\u0142asna");
		lblWpataWasna.setForeground(new Color(204, 204, 204));
		lblWpataWasna.setFont(new Font("Bookman Old Style", Font.ITALIC, 18));
		add(lblWpataWasna, "cell 1 5,growx,aligny top");

		textField_1 = new JTextField();
		textField_1.setBackground(new Color(204, 204, 204));
		textField_1.setFont(new Font("Bookman Old Style", Font.BOLD, 15));
		textField_1.setColumns(10);
		add(textField_1, "cell 1 6,grow");

		JLabel lblUbezpieczenie = new JLabel("Ubezpieczenie");
		lblUbezpieczenie.setForeground(new Color(204, 204, 204));
		lblUbezpieczenie.setFont(new Font("Bookman Old Style", Font.ITALIC, 18));
		add(lblUbezpieczenie, "cell 1 7,growx,aligny top");

		JLabel lblPromocja = new JLabel("Promocja");
		lblPromocja.setForeground(new Color(204, 204, 204));
		lblPromocja.setFont(new Font("Bookman Old Style", Font.ITALIC, 18));
		add(lblPromocja, "cell 3 7,alignx left,aligny top");

		comboBox = new JComboBox<String>();
		comboBox.setBackground(new Color(204, 204, 204));
		comboBox.setFont(new Font("Bookman Old Style", Font.BOLD, 15));
		comboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String text = comboBox.getSelectedItem().toString();
				if (text.equals("Brak ubezpieczenia"))
					ubezpieczenie = 0;
				if (text.equals("1 %"))
					ubezpieczenie = 1;
				if (text.equals("5 %"))
					ubezpieczenie = 5;
				if (text.equals("10 %"))
					ubezpieczenie = 10;
				if (text.equals("20 %"))
					ubezpieczenie = 20;
			}
		});
		comboBox.addItem("Brak ubezpieczenia");
		comboBox.addItem("1 %");
		comboBox.addItem("5 %");
		comboBox.addItem("10 %");
		comboBox.addItem("20 %");
		add(comboBox, "cell 1 8,growx,aligny center");

		comboBox_1 = new JComboBox<String>();
		comboBox_1.setBackground(new Color(204, 204, 204));
		comboBox_1.setFont(new Font("Bookman Old Style", Font.BOLD, 15));
		comboBox_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String text = comboBox_1.getSelectedItem().toString();
				if (text.equals("Standard"))
					promocja = 0.02;
				if (text.equals("1,5 %"))
					promocja = 0.015;
				if (text.equals("1 %"))
					promocja = 0.01;
				if (text.equals("0,5 %"))
					promocja = 0.005;
			}
		});
		comboBox_1.addItem("Standard");
		comboBox_1.addItem("1,5 %");
		comboBox_1.addItem("1 %");
		comboBox_1.addItem("0,5 %");
		add(comboBox_1, "cell 3 8,alignx left,aligny center");

		JLabel lblLiczbaRat = new JLabel("LICZBA RAT");
		lblLiczbaRat.setForeground(new Color(204, 204, 204));
		lblLiczbaRat.setFont(new Font("Bookman Old Style", Font.ITALIC, 24));
		add(lblLiczbaRat, "cell 1 10,growx,aligny top");

		JLabel lblKwotaRaty = new JLabel("KWOTA RATY");
		lblKwotaRaty.setForeground(new Color(204, 204, 204));
		lblKwotaRaty.setFont(new Font("Bookman Old Style", Font.ITALIC, 24));
		add(lblKwotaRaty, "cell 3 10,growx,aligny top");

		textField_4 = new JTextField();
		textField_4.setBackground(new Color(204, 204, 204));
		textField_4.setFont(new Font("Bookman Old Style", Font.BOLD, 18));
		textField_4.setColumns(10);
		add(textField_4, "cell 1 11,grow");

		textField_5 = new JTextField();
		textField_5.setBackground(new Color(204, 204, 204));
		textField_5.setFont(new Font("Bookman Old Style", Font.BOLD, 18));
		textField_5.setColumns(10);
		add(textField_5, "cell 3 11,grow");

		JLabel lblProgres = new JLabel("Progres...");
		lblProgres.setForeground(new Color(204, 204, 204));
		lblProgres.setFont(new Font("Bookman Old Style", Font.BOLD | Font.ITALIC, 14));
		add(lblProgres, "cell 1 13 3 1,alignx center,aligny top");

		progressBar = new JProgressBar(0, 100);
		add(progressBar, "cell 1 14 3 1,grow");
		
				JButton btnOblicz = new JButton("OBLICZ");
				btnOblicz.setForeground(new Color(0, 0, 0));
				btnOblicz.setBackground(new Color(102, 102, 102));
				btnOblicz.setFont(new Font("Bookman Old Style", Font.BOLD | Font.ITALIC, 23));
				btnOblicz.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						label_1.setText("");
						double kwota = Double.parseDouble(textField.getText());
						double wplata = Double.parseDouble(textField_1.getText());
						if (textField_5.getText().isEmpty()) {
							int lR = Integer.parseInt(textField_4.getText());
							try {
								AplikacjaKredytowa.obliczKR(kwota, wplata, lR, promocja, ubezpieczenie);
								oKR = new ProgressBar();
								oKR.start();
								ProgressBar.timer.start();
							} catch (IOException e1) {
								e1.printStackTrace();
							}
						}
						else if (textField_4.getText().isEmpty()) {
							double kR = Double.parseDouble(textField_5.getText());
							try {
								AplikacjaKredytowa.obliczLR(kwota, wplata, kR, promocja, ubezpieczenie);
								oIR = new ProgressBar();
								oIR.start();
								ProgressBar.timer.start();
							} catch (IOException e1) {
								e1.printStackTrace();
							}
						}
						else if (!textField_5.getText().isEmpty() && !textField_4.getText().isEmpty()) {
							label_1.setText("Wyczy�� kom�rk� LICZBA RAT lub KWOTA RATY");
						}
					}
				});

				
				label_1 = new JLabel("");
				label_1.setForeground(new Color(204, 204, 204));
				label_1.setFont(new Font("Bookman Old Style", Font.BOLD, 14));
				add(label_1, "cell 1 16 2 1");
				btnOblicz.setFont(new Font("Bookman Old Style", Font.BOLD | Font.ITALIC, 23));
				add(btnOblicz, "cell 3 16,alignx right,aligny top");

	}
}
