package GUI;

import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import net.miginfocom.swing.MigLayout;
import java.awt.Color;

public class ListaPanel extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static JTable table;
	public static JScrollPane scrollPane;
	public static JButton button;
	static int numOfRows;

	static String[] columnNames = { "ID", "NAZWISKO", "IMI�", "PESEL", "DATA", "STATUS", "WY�LIJ" };
	private JLabel label;

	/**
	 * Create the panel.
	 * 
	 * @throws UnsupportedLookAndFeelException
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 * @throws ClassNotFoundException
	 */
	public ListaPanel() throws ClassNotFoundException, InstantiationException, IllegalAccessException,
			UnsupportedLookAndFeelException {
		setBackground(new Color(25, 25, 25));
		UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
		setBounds(0, 0, 960, 870);
		setLayout(new MigLayout("", "[100.00px,grow][100.00,grow][100.00,grow][100.00,grow][100.00,grow][100.00,grow][100.00,grow][100.00,grow][100.00px,grow]", "[60.00][50px][][15.00][500.00]"));
		
		label = new JLabel("");
		add(label, "cell 4 0");

		JLabel lblSymulacjaWniosku1 = new JLabel("LISTA WNIOSK�W");
		lblSymulacjaWniosku1.setForeground(new Color(204, 204, 204));
		lblSymulacjaWniosku1.setFont(new Font("Bookman Old Style", Font.BOLD | Font.ITALIC, 40));
		lblSymulacjaWniosku1.setHorizontalAlignment(SwingConstants.CENTER);
		lblSymulacjaWniosku1.setBounds(260, 207, 480, 53);
		add(lblSymulacjaWniosku1, "cell 1 1 7 1,growx,aligny center");

		table = new JTable();
		table.setFont(new Font("Bookman Old Style", Font.BOLD, 13));
		table.getTableHeader().setFont(new Font("Bookman Old Style", Font.BOLD, 13)); 
		table.getTableHeader().setForeground(new Color(51, 51, 51));
		table.setForeground(new Color(51, 51, 51));
		table.setBackground(new Color(240, 240, 240));
		scrollPane = new JScrollPane(table);
		scrollPane.setEnabled(false);
		add(scrollPane, "cell 1 4 7 1,growx,aligny top");
	}
}
