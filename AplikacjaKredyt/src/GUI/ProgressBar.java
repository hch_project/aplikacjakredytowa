package GUI;

import javax.swing.*;

import java.awt.event.*;

public class ProgressBar extends Thread {
	// the objects

	int progress;
	public static Timer timer;

	// constructor where we initializes
	public ProgressBar() {

		SymulacjaPanel.progressBar.setValue(0);
		SymulacjaPanel.progressBar.setStringPainted(true);

		progress = 0;

		timer = new Timer(30, new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// if i is equal to 100, the progress is already 100% so we stop
				if (progress == 100) {
					// set the value to zero and stop the timer obj
					timer.stop();
					SymulacjaPanel.progressBar.setValue(0);
					if (SymulacjaPanel.textField_4.getText().isEmpty()) {
						SymulacjaPanel.textField_4.setText(String.valueOf(SymulacjaPanel.liczbaRat));
						SymulacjaPanel.oIR.interrupt();
					}
					if (SymulacjaPanel.textField_5.getText().isEmpty()) {
						SymulacjaPanel.textField_5.setText(SymulacjaPanel.kwotaRaty.toString() + " z�");
						SymulacjaPanel.oKR.interrupt();
					}
				}
				// this will increase the progressbar
				progress += 1;
				SymulacjaPanel.progressBar.setValue(progress);

			}
		});
	}

}